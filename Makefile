PREFIX = $(DESTDIR)/usr

all:
	@echo "Nothing to make, use make install"

install:
	install -D -m 755 daemon/minio $(PREFIX)/sbin/minio
	install -D -m 755 client/mc $(PREFIX)/bin/minio-mc
